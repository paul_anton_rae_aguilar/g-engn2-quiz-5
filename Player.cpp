#include "Player.h"
#include <iostream>

Player::Player(float x,float y):Object(x,y,0.5f,0.5f,1,1,1){
    moving = true;
    hasJumped = false;
}

void Player::jump(){
    if(!hasJumped){
		objVec.setYVec(0.25f);
        hasJumped = true;
    }
}

void Player::land(){
    hasJumped = false;
}

void Player::moveLeft(){
	objVec.setXVec(-0.1f);
}

void Player::moveRight(){
	objVec.setXVec(0.1f);
}

void Player::haltHorizontal(){
	objVec.setXVec(0.0f);
}

void Player::setY(float val){
    yPos = val;
    transform.GetPos()->y = yPos;
}

void Player::setX(float val){
    xPos = val;
    transform.GetPos()->x = xPos;
}
